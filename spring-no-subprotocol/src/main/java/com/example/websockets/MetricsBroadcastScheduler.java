package com.example.websockets;

import com.example.websockets.model.MetricsType;
import com.example.websockets.service.HardwareMetricsService;
import com.example.websockets.socket.SocketHandler;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class MetricsBroadcastScheduler {

  private final HardwareMetricsService metricsService;
  private final SocketHandler socketHandler;
  private final ObjectMapper objectMapper;

  public MetricsBroadcastScheduler(
      final HardwareMetricsService metricsService,
      final SocketHandler socketHandler,
      final ObjectMapper objectMapper) {
    this.metricsService = metricsService;
    this.socketHandler = socketHandler;
    this.objectMapper = objectMapper;
  }

  @Scheduled(fixedDelayString = "${app.metrics.broadcast.delay}")
  public void broadcast() throws JsonProcessingException {
    for (final var type : MetricsType.values()) {
      broadcastCurrentMetrics(type);
    }
  }

  private void broadcastCurrentMetrics(final MetricsType type) throws JsonProcessingException {
    final var metrics = metricsService.getCurrent(type);
    final String json = objectMapper.writeValueAsString(metrics);
    socketHandler.broadcast(json);
  }

}
