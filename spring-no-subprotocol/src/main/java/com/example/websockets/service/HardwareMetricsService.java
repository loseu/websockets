package com.example.websockets.service;

import com.example.websockets.model.HardwareMetrics;
import com.example.websockets.model.MetricsType;

public interface HardwareMetricsService {

  HardwareMetrics getCurrent(MetricsType type);

}
