package com.example.websockets.service;

import com.example.websockets.model.HardwareMetrics;
import com.example.websockets.model.MetricsType;
import com.example.websockets.util.DateTimeUtils;
import java.time.LocalDateTime;
import org.springframework.stereotype.Service;

@Service
public class HardwareMetricsServiceImpl implements HardwareMetricsService {

  @Override
  public HardwareMetrics getCurrent(final MetricsType type) {
    final LocalDateTime now = DateTimeUtils.getCurrentDateTime();
    final double randomValue = Math.random();
    return HardwareMetrics.builder()
        .dateTime(now)
        .type(type)
        .usage(randomValue)
        .build();
  }

}
