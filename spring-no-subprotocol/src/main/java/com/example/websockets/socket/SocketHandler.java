package com.example.websockets.socket;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

@Component
public class SocketHandler extends TextWebSocketHandler {

  private static final Logger LOGGER = LoggerFactory.getLogger(SocketHandler.class);

  private final List<WebSocketSession> sessions = new CopyOnWriteArrayList<>();

  @Override
  public void afterConnectionEstablished(final WebSocketSession session) throws Exception {
    sessions.add(session);
  }

  @Override
  public void afterConnectionClosed(
      final WebSocketSession session,
      final CloseStatus status
  ) throws Exception {
    sessions.remove(session);
  }

  @Override
  public void handleMessage(
      final WebSocketSession session,
      final WebSocketMessage<?> message
  ) throws Exception {
    LOGGER.info("Message received: " + message);
  }

  public void broadcast(final String text) {
    final var message = new TextMessage(text);
    sessions.forEach(session -> sendMessage(session, message));
  }

  private void sendMessage(final WebSocketSession session, final WebSocketMessage<?> message) {
    try {
      session.sendMessage(message);
    } catch (IOException e) {
      LOGGER.warn("Message was not sent to " + session.getId(), e);
    }
  }

}
