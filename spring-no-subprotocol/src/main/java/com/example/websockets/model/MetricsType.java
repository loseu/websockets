package com.example.websockets.model;

public enum MetricsType {

  CPU, MEMORY, DISK

}
