package com.example.websockets.model;

import java.time.LocalDateTime;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class HardwareMetrics {

  private final LocalDateTime dateTime;
  private final MetricsType type;
  private final Double usage;

}
