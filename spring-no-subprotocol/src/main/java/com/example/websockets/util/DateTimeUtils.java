package com.example.websockets.util;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

public interface DateTimeUtils {

  static LocalDateTime getCurrentDateTime() {
    return LocalDateTime.now(ZoneOffset.UTC);
  }

}
